## MOMW Camera Changelog

#### Version 2.1

* Added the missing translation file folder

[Download Link](https://gitlab.com/modding-openmw/momw-camera/-/packages/23880353)

#### Version 2.0

* Rewrote how the mod works:
  * Changes are now only applied once, when you start a new game
  * To re-apply changes at any time, navigate to the script settings menu (ESC >> Options >> Scripts >> MOMW Camera) and set "Factory Reset" to "yes"

[Download Link](https://gitlab.com/modding-openmw/momw-gameplay/-/packages/23717288)

#### Version 1.2

Added a print to the console for visibility.

[Download Link](https://gitlab.com/modding-openmw/momw-camera/-/packages/10820692)

#### Version 1.1

Some very minor code cleanup.

[Download Link](https://gitlab.com/modding-openmw/momw-camera/-/packages/10796947)

#### Version 1.0

Initial version of the mod.

[Download Link](https://gitlab.com/modding-openmw/momw-camera/-/packages/10796233)
