local async = require("openmw.async")
local storage = require('openmw.storage')
local I = require("openmw.interfaces")

local thirdPerson = storage.playerSection("SettingsOMWCameraThirdPerson")
local headBobbing = storage.playerSection("SettingsOMWCameraHeadBobbing")

local MOD_ID = "MOMW_Camera"
local SETTINGS_KEY = 'Settings' .. MOD_ID

I.Settings.registerPage {
    key = MOD_ID,
    l10n = MOD_ID,
    name = "name",
    description = "description"
}

I.Settings.registerGroup {
    key = SETTINGS_KEY,
    page = MOD_ID,
    l10n = MOD_ID,
    name = "settingsTitle",
    permanentStorage = false,
    settings = {
        {
            key = 'factoryReset',
            name = "factoryReset_name",
            description = "factoryReset_desc",
            default = false,
            renderer = 'checkbox'
        }
    }
}

local function applyAll()
    thirdPerson:set("autoSwitchShoulder", true)        -- false
    thirdPerson:set("deferredPreviewRotation", true)   -- false
    thirdPerson:set("ignoreNC", true)                  -- false
    thirdPerson:set("previewIfStandStill", true)       -- false
    thirdPerson:set("slowViewChange", true)            -- false
    thirdPerson:set("viewOverShoulder", true)          -- false
    thirdPerson:set("zoomOutWhenMoveCoef", 60)         -- 20
    headBobbing:set("enabled", true) -- false
    headBobbing:set("step", 120)     -- 90
    headBobbing:set("height", 6)     -- 3
    headBobbing:set("roll", 0.5)     -- 0.2
    print("MOMW Camera: settings have been applied")
end

local function factoryReset(_, key)
    if key == "factoryReset" and storage.playerSection(SETTINGS_KEY):get("factoryReset") == true then
        applyAll()
        print("MOMW Camera: factory reset executed")
    end
end
storage.playerSection(SETTINGS_KEY):subscribe(async:callback(factoryReset))

return {engineHandlers = {onInit = applyAll}}
