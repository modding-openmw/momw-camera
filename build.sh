#!/bin/sh
set -e

file_name=momw-camera.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} scripts l10n LICENSE CHANGELOG.md README.md momw-camera.omwscripts version.txt
sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
