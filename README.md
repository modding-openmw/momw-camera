# MOMW Camera

Automatically configure third person camera and head bobbing settings with the Modding-OpenMW.com recommended values.

**Requires OpenMW 0.48 or newer!**

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/momw-camera/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Camera\momw-camera

        # Linux
        /home/username/games/OpenMWMods/Camera/momw-camera

        # macOS
        /Users/username/games/OpenMWMods/Camera/momw-camera

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Camera\momw-camera"`)
1. Add `content=momw-camera.omwscripts` to your load order in `openmw.cfg` or enable it via OpenMW-Launcher
1. Each time you run OpenMW, the following camera and head bobbing settings will be set:
    * View over the shoulder: **Yes**
    * Auto switch shoulder: **Yes**
    * Zoom out when move coef: **60**
    * Preview if stand still: **Yes**
    * Ignore 'No Collision' flag: **Yes**
    * Smooth view change: **Yes**
    * Head bobbing enabled: **Yes**
    * Head bobbing base step length: **120**
    * Head bobbing step height: **6**
    * Head bobbing max roll angle: **0.5**

In order to customize these settings you must disable this mod (it is safe to disable or re-enable at any time mid-save).

#### Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/momw-camera/-/issues)
* Email `momw-camera@modding-openmw.com`
* Contact the author on Discord: `johnnyhostile#6749`
* Contact the author on Libera.chat IRC: `johnnyhostile`
